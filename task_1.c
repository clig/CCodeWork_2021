// 1)、完成一个大小写字母自动转换的程序，输入大写字母，程序可以输出小写字母，输入小写字母，可以输出大写字母

// #include <stdio.h>
// int main(void)
// {
//     char x;
//     printf("输入一个字母：");
//     scanf("%c", &x);
//     if (x >= 'a')
//         x -= 'a' - 'A';
//     else
//         x += 'a' - 'A';
//     printf("转换为：%c\n", x);
//     printf("%c", x);
// }

#include <stdio.h>
#include <string.h>
#define buffer 1024
int main(void)
{
    int i;
    char a[buffer];
    printf("大小写字母自动转换程序\n");
    printf("小写字母变大写字母，大写字母变小写字母\n");
    printf("回车结束输入\n");
    while (scanf("%s", a) != EOF)
    {
        for (i = 0; i < strlen(a); i++)
        {
            if (a[i] >= 'A' && a[i] <= 'Z')
            {
                a[i] += 32;
            }
            else if (a[i] >= 'a' && a[i] <= 'z')
            {
                a[i] -= 32;
            }
        }
        printf("转换为：%s\n", a);
    }
    return 0;
}

// cd "/Applications/2021年秋《C语言程序设计》主观题作业/2021_C_CODE_WORK/CCodeWork_2021/" && gcc task_1.c -o task_1 && "/Applications/2021年秋《C语言程序设计》主观题作业/2021_C_CODE_WORK/CCodeWork_2021/"task_1