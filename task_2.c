// 2)、用三种循环方式实现1-100的和计算(提示：do...while, while,for)

#include <stdio.h>
int main(void)
{
    printf("——————————————使用 do...while 循环——————————————\n");
    int n_do = 0, sum_do = 0;
    do
    {
        n_do++;
        sum_do += n_do;
    } while (n_do < (int)100);
    printf("1+2+...+100=%d\n", sum_do);
    printf("—————————————— 使用 while 循环 ——————————————\n");
    int n_while = 0, sum_while = 0;
    while (n_while < (int)100)
    {
        n_while++;
        sum_while += n_while;
    };
    printf("1+2+...+100=%d\n", sum_while);
    printf("—————————————— 使用 for 循环 ——————————————\n");
    int n_for, sum_for = 0;
    for (n_for = 1; n_for <= 100; n_for++)
        sum_for += n_for;
    printf("1+2+...+100=%d\n", sum_for);
    return 0;
}

// cd "/Applications/2021年秋《C语言程序设计》主观题作业/2021_C_CODE_WORK/CCodeWork_2021/" && gcc task_2.c -o task_2 && "/Applications/2021年秋《C语言程序设计》主观题作业/2021_C_CODE_WORK/CCodeWork_2021/"task_2