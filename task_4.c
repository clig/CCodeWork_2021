// 4)、用指针实现一个字符串输入，然后把输入字符串倒序输出的程序
#include <stdio.h>
#include <string.h>
int main()
{
    int i, j, t, n;
    char a[10];
    printf("请输入字符串:");
    gets(a);
    n = strlen(a);
    for (i = 0; i <= n / 2; i++)
    {
        t = a[i];
        a[i] = a[n - 1 - i];
        a[n - 1 - i] = t;
    }
    for (j = 0; j < n; j++)
        printf("%c", a[j]);
    printf("\n");
}

// cd "/Applications/2021年秋《C语言程序设计》主观题作业/2021_C_CODE_WORK/CCodeWork_2021/" && gcc task_4.c -o task_4 && "/Applications/2021年秋《C语言程序设计》主观题作业/2021_C_CODE_WORK/CCodeWork_2021/"task_4