// 3)、说说数组和指针的区别

#include <stdio.h>
int main(void)
{
    printf("指针就是指针，指针变量在32位的系统下面是4Byte，而在64位系统下面是8Byte，其值为某一个内存的地址。而数组就是数组，其大小与元素的类型和个数有关，定义数组时必须制定其元素的类型和个数，数组可以存放任何类型的数据，但是不能存放函数。\n");
    printf("指针数组：首先它是一个数组，这个数组全是指针，数组占用多少个字节有数组本身决定。它是“储存指针的数组”的简称。\n");
    printf("数组指针：首先它是一个指针，它指向一个数组。在32位系统下永远占用4个字节，至于它指向的数组占多少个字节，不知道。它是“指向数组的指针”的简称。\n");
}

// cd "/Applications/2021年秋《C语言程序设计》主观题作业/2021_C_CODE_WORK/CCodeWork_2021/" && gcc task_3.c -o task_3 && "/Applications/2021年秋《C语言程序设计》主观题作业/2021_C_CODE_WORK/CCodeWork_2021/"task_3